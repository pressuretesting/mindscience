sponge.function.set_global_length_unit
==========================================

.. py:function:: sponge.function.set_global_length_unit(unit)

    给全局单位设置长度单位。

    参数：
        - **unit** (str) - 长度单位。